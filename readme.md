# Scheduler

Schedulers on a single processor.

## Part A: Implementation

Implement the following scheduling:

- FCFS, first-come, first-served scheduling
- SJN, shortest-job-next scheduling
- Shortest-remaining-time-first scheduling
- Priority scheduling
- Round-robin scheduling 

### Input Data Form

```
[Process ID, Priority, execution time, arriving time]
```

#### Priority
- The smaller the priority digit is, the higher the priority
- SJN scheduling considers the reciprocal of execution time as priority
- Round-robin scheduling assumes all processes arrive at the same time and are available to be scheduled

#### Testing Data Set

All testing data are in Poisson distribution.

1. `data_1.txt`
	- 10000 data
	- More processes but in short execution time

2. `data_2.txt`
	- 1000 data
	- In long execution time but less processes


## Part B: Efficiency Comparison

Record and compare the following info of all scheduling method.

- Numbers of context switch
- Longest waiting time, average waiting time
	- Judge if starvation and infinite blocking occur
- Number or ratio of completed tasks in certain time period

### Goal

Argue the efficiency of different schedulers and the appropriate circumstances to apply which of them.

### Requirement

Record and analysis the efficiency of schedulers indicated in part A.

1. Waiting time, average waiting time, whether starvation occurs and whether infinite blocking occurs.
2. Turnaround time
3. Throughput (completed tasks in an unit of time)

#### Available Condition

- Assume the time consumption of context switch
	- Consider different assumption of time
	- Compare different scheduling method
- Assume the total available process time
	- Analysis the ratio of completed processes in an unit of time
- Assume a switch period in round-robin scheduling

## Part C: Schedulers on Multiprocessor

### Goal

Acknowledge the concept of multiprocessor scheduling.

- SMP architecture
- ASMP architecture

### Requirement

Reference a paper , a technique document or a scheduling method in effect to introduce the scheduling on multiprocessor.

The report must contains

- Reference
- Concept of scheduling on multiprocessor
- Work flow of scheduling on multiprocessor
- Calculation of time complexity 
