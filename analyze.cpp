/*
 * Copy Right Reserved.
 *
 * NCKU EE Department, OS
 *
 * Date: 2019/05/03
 */

#include <iostream>
#include <fstream>
#include <queue>
#include <vector>
#include <algorithm>
#include <math.h>
#include <string>
#include <iomanip>    // Needed for stream modifiers fixed and setprecision

// #include <bits/stdc++.h>

#define INPUTDATA  "data_2.txt"
// #define OUTPUTDATA "output.txt"
const char* OUTPUTDATA[5] {
	"FCFS.csv",
	"SJN.csv",
	"SRTF.csv",
	"PRY.csv",
	"RR.csv"
};
const char* THROUGHPUT[5] {
	"FCFS_TP.csv",
	"SJN_TP.csv",
	"SRTF_TP.csv",
	"PRY_TP.csv",
	"RR_TP.csv"
};
const char* WAITTIME[5] {
	"FCFS_WT.csv",
	"SJN_WT.csv",
	"SRTF_WT.csv",
	"PRY_WT.csv",
	"RR_WT.csv"
};


// The time quantum in round-robin algorithm
#define TIMEQUANTUM 1

using namespace std;

typedef struct process {
    int pid;   // process id
    int priority;
    int exe_time; // execution time
    int arv_time; // arriving time
    int trm_time; // terminated time
	int bst_time; // CPU burst time
} process;

/*
 * Scheduling Function
 */
void schedDecision(int choose, queue<process> data, vector<process> &result);
void FCFS(queue<process>, vector<process>&);
void SJN (queue<process>, vector<process>&);
void SRTF(queue<process>, vector<process>&);
void PRY (queue<process>, vector<process>&);
void RR  (queue<process>, vector<process>&);

/*
 * Analyze Function
 */
float avgTimeWait(vector<process> result, unsigned int  &max_wait_time);
void waitTime(vector<process> result, vector<vector<unsigned int> > &ans);
float turnAround(vector<process> result);
float totalThroughPut(vector<process> result);
vector<double> throughPut(vector<process> result) ;
unsigned int contextSwitch(vector<process> result);
float reactTime(vector<process> result, const unsigned int data_size);

/*
 * Operating Function
 */
bool compare_priority(process p1, process p2);
void readFile(string in_file_name, queue<process> &input_data);
void saveToFile(string out_file_name, vector<process> &sched);
void saveTP(string out_file_name, vector<double> &sched);
void saveWT(string out_file_name, vector<vector<unsigned int> > &sched);
void insertAndSort(vector<process> &vec, process new_member);
void InsertionSort(vector<process> &ready);
void giveVal(process &p1, process &p2);
int turnToNum(string s);


int main() {

    queue<process> data;
    vector<process> sched;   // record the scheduled result
	vector<double> through_put;
    float avg_wait_time = 0;
    float turn_around_time = 0;
    float total_through_put = 0;
	float react_time = 0;
	unsigned int max_wait_time = 0;

    /********************** Read Porcesses Information ************************/
    
    readFile(INPUTDATA, data);
        
    /****************************** Scheduling ********************************/

    for (int i = 0; i < 5; i++)
    {
        sched.clear();
		through_put.clear();
		vector <vector<unsigned int> > WT_v;
		// vector <vector<unsigned int> > WT_v(data.size(), vector<unsigned int> (2));
		max_wait_time = 0;
        /*
         * This function choose which scheduling method to use
         *  0 => FCFS
         *  1 => SJN
         *  2 => SRTF
         *  3 => PRY
         *  4 => RR
         */
        schedDecision(i, data, sched);

        if (!sched.empty()) {
            cout << "Scheduler " << i;

            avg_wait_time = avgTimeWait(sched, max_wait_time);
            cout << "\tAvg Waiting Time: " << avg_wait_time;
            cout << "\tMax Waiting Time: " << max_wait_time;
			waitTime(sched, WT_v);

            turn_around_time = turnAround(sched);
            cout << "\tAvg Turnaround Time: " << turn_around_time;

            // through_put = (float) data.size() / sched[sched.size()-1].trm_time;
            total_through_put = totalThroughPut(sched);
            cout << "\tTotal Throughput: " << total_through_put;
			through_put = throughPut(sched);
			react_time = reactTime(sched, data.size());
            cout << "\tAvg React Time: " << react_time;

            cout << "\tContext Switch: " << sched.size();

            cout << endl;

        }
        saveToFile(OUTPUTDATA[i], sched);
		saveTP(THROUGHPUT[i], through_put);
		saveWT(WAITTIME[i], WT_v);
		// for (int i = 0; i < WT_v.size(); i++)
		// {
		// 	cout<< WT_v[i][0] << " " << WT_v[i][1] << endl;
		// }
		

    }
    
    /*********************** Output Porcesses Result **************************/
    // saveToFile(OUTPUTDATA, sched);

    return 0;
}


/****************************************************************************
 *                          SCHEDULING  FUNCTION                            *
 ****************************************************************************/

/*
 * Used to choose schedule to run
 *  0 => FCFS
 *  1 => SJN
 *  2 => SRTF
 *  3 => PRY
 *  4 => RR
 */
void schedDecision(int choose, queue<process> data, vector<process> &result) {
    if (choose == 0) {
        /* FCFS */
		FCFS(data, result);
    }
    else if (choose == 1) {
        /* SJN */
        SJN(data, result);
    }
    else if (choose == 2) {
        /* SRTF */
        SRTF(data, result);
    }
    else if (choose == 3) {
        /* PRY */
        PRY(data, result);
    }
    else if (choose == 4) {
        /* RR */
		RR(data, result);
    }
    else {
        cout<<"Error cmd\n";
    }
}

/*
 * First-Come-First-Serve Scheduling Algorithm
 * The job which comes first run first
 */
void FCFS(queue <process> data, vector <process> &sched) {
	queue <process> ready;
	vector <process> arrive;	// record the process arrives at the same time
	process ps;					// temp process for priority sort

	int time_slot = -1;
	process running_ps = { 0, 0, 0, 0, -1, 0 };
	// init terminate time as -1 to avoid pushing an empty process to sched Q

	// execute all the processes & jump out if running process has terminated
	while (!data.empty() || !ready.empty() || running_ps.trm_time > time_slot) {

		// as time goes
		time_slot++;
		running_ps.exe_time = (running_ps.exe_time > 0) ? (running_ps.exe_time - 1) : 0;

		// if a new process arrives in this time slot, put it in ready queue
		// Note: The same arriving time may occur
		while (!data.empty() && data.front().arv_time == time_slot) {
			ps = data.front();
			arrive.push_back(ps);
			data.pop();
		}

		// if several ps arrive at the same time slot, sort based on priority
		switch (arrive.size()) {
			case 0:
				break;
			case 1:
				ps = arrive.at(0);
				ready.push(ps);
				arrive.clear();
				break;
			default:
				// cerr << "Multi-arriving at " << arrive.at(1).pid << "\n";
				sort(arrive.begin(), arrive.end(), compare_priority);
				for (int i = 0; i < arrive.size(); i++) {
					ps = arrive.at(i);
					ready.push(ps);
				}
				arrive.clear();
		}

		// if a running process terminate, record it
		if (running_ps.trm_time == time_slot) {
			sched.push_back(running_ps);
			// cout << running_ps.pid << " terminated.\n";
		}
		// if the ready queue is not empty, execute the first process in it
		if (!ready.empty() && running_ps.trm_time <= time_slot) {
			if (ready.front().arv_time <= time_slot) {
				running_ps = ready.front();
				running_ps.trm_time = time_slot + running_ps.bst_time;
				ready.pop();
			}
		}
	}
}


/*
 * Shortest-Job-Next Scheduling Algorithm
 * The job which has the shortest execute time run first
 */
void SJN(queue<process> data, vector<process> &result) {
    vector<process> ready;        //Simulate ready queue
    process exec = {-1, 0, 0, 0, 0, -1};
	process emt = {-1, 0, 0, 0, 0, -1}; // empty execution process
	
    int NOW = 0;
    while(!data.empty() || !ready.empty() || exec.exe_time!=0) {
        NOW++;
        exec.trm_time = NOW;
        
        // The porcess has done another cpu burst
		if (exec.exe_time > 0) 
			exec.exe_time--;
		
        // Sort the incomming process when the process arrives
        while(!data.empty() && data.front().arv_time == NOW) {
            insertAndSort(ready, data.front());
            data.pop();
        }

        // If the process is done,
        // record and execute the next process
        if (exec.exe_time == 0) {
            if (exec.pid >0) {
                result.push_back(exec); 
            }
			giveVal(exec, emt);
			if ( ready.size() > 0 ) {
				// move process from ready queue to execute
				giveVal(exec, ready[ready.size()-1]);
				ready.pop_back();
			}
        }
    }
}

/*
 * Shortest-Job-Next Scheduling Algorithm
 * The job which has the shortest execute time run first
 */
void SRTF(queue<process> data, vector<process> &result) {
    vector<process> ready;        //Simulate ready queue
    process exec = {-1, 0, 0, 0, 0, -1};
	process emt = {-1, 0, 0, 0, 0, -1}; // empty execution process
    int NOW = 0;
    while(!data.empty() || !ready.empty() || exec.exe_time!=0) {
        NOW++;
        exec.trm_time = NOW;

        // The porcess has done another cpu burst
		if (exec.exe_time > 0) 
			exec.exe_time--;
		
		// Put the arrive process into ready queue
        while(!data.empty() && data.front().arv_time == NOW) {
            insertAndSort(ready, data.front());
            data.pop();
        }
		
        // If the process is done,
        // record and execute the next process
        if (exec.exe_time == 0) {
			if (exec.pid >0) {      // if this is not an empty process
                result.push_back(exec);
            }
			giveVal(exec, emt);
			if ( ready.size() > 0 ) {
				// move process from ready queue to execute
				giveVal(exec, ready[ready.size()-1]);
				ready.pop_back();
				continue;
			}
        }

        // If the process is not done yet,
        // need to go into the ready queue to sort again 
		if (exec.exe_time >0) {
			insertAndSort(ready, exec);
			if (ready[ready.size()-1].pid != exec.pid) {
		        result.push_back(exec);
				giveVal(exec, ready[ready.size()-1] );
			}
			ready.pop_back();
		}
        
    }
    
}

/*
 * Priority Scheduling Algorithm
 * The job which has the smallest priority run first
 */
void PRY(queue<process> data, vector<process> &sched) {
	vector<process> ready;
	process        ps;

	// init terminate time as -1 to avoid pushing an empty process to sched Q
	int time_slot = -1;
	process running_ps = { 0, 0, 0, 0, 0, 0 };

	// execute all the processes & jump out if running process has terminated
	while (!data.empty() || !ready.empty() || running_ps.exe_time != 0) {

		// as time goes
		time_slot++;

		// decrease the execute time of runnung process
		if (running_ps.exe_time > 0) {
			running_ps.exe_time--;
		}
		// if it terminate, pop out and record it
		if (running_ps.exe_time == 0 && running_ps.trm_time == -1) {
			running_ps.trm_time = time_slot;
			sched.push_back(running_ps);
		}

		// if a new process arrives in this time slot, put it in ready queue
		while (!data.empty() && data.front().arv_time == time_slot) {
			ps = data.front();
			ready.push_back(ps);
			data.pop();
		}

		// if the ready queue is not empty, select the process with the smallest priority to execute
		if (!ready.empty() && running_ps.exe_time <= 0) {
			//  Move the smallest priority one to the last one of vector for pop_back
			InsertionSort(ready);
			// sort(ready.begin(), ready.end(), compare_priority);
			if (ready.back().arv_time <= time_slot) {
				running_ps = ready.back();
				ready.pop_back();
			}
		}
	}
}

/*
 * Round-Robin Scheduler
 * The job which is not completed will be preempted in certain time quantum
 */
void RR(queue <process> data, vector < process > &sched)
{
	queue <process> ready;
	vector <process> arrive;	// record the process arrives at the same time
	process ps;					// temp process for priority sort
	int time_qtm = -1;          // time quantum
	bool available = true;

	int time_slot = -1;
	process running_ps = { 0, 0, 0, 0, -1, 0 };
	// init terminate time as -1 to avoid pushing an empty process to sched Q

	// execute all the processes & jump out if running process has terminated
	while (!data.empty() || !ready.empty() || running_ps.trm_time > time_slot) {

		// as time goes
		time_qtm++;
		time_slot++;
		running_ps.exe_time =
			(running_ps.exe_time > 0) ? (running_ps.exe_time - 1) : 0;

		// if a new process arrives in this time slot, put it in ready queue
		// Note: The same arriving time may occur
		while (!data.empty() && data.front().arv_time == time_slot) {
			ps = data.front();
			arrive.push_back(ps);
			data.pop();
		}
		
		// if several ps arrive at the same time slot, sort based on priority
		switch (arrive.size()) {
			case 0:
				break;
			case 1:
				ps = arrive.at(0);
				ready.push(ps);
				arrive.clear();
				break;
			default:
				// cerr << "Multi-arriving at " << arrive.at(1).pid << "\n";
				sort(arrive.begin(), arrive.end(), compare_priority);
				for (int i = 0; i < arrive.size(); i++) {
					ps = arrive.at(i);
					ready.push(ps);
				}
				arrive.clear();
		}

		// if a running process terminate, record it
		if (running_ps.trm_time == time_slot) {
			sched.push_back(running_ps);
			// cout << running_ps.pid << " terminated.\n";

			// mark that the CPU to be available
			available = true;
		}

		// if the ready queue is not empty, and the time expired
		// push the running process to the ready queue
		if (time_qtm >= TIMEQUANTUM) {
			if (running_ps.exe_time > 0) {
				// wait for the next execution
				ready.push(running_ps);

				// record the time period
				sched.push_back(running_ps);
				sched.at(sched.size() - 1).trm_time = time_slot;

				// mark that the CPU to be available
				available = true;
			}
		}

		// if the ready queue is not empty, execute the first process in it
		if (!ready.empty() && available) {
			if (ready.front().arv_time <= time_slot) {
				running_ps = ready.front();
				running_ps.trm_time = time_slot + running_ps.exe_time;
				ready.pop();

				available = false;

				// reset the time quantum counter
				time_qtm = 0;
			}
		}
	}
}
/****************************************************************************
 *                            ANALYZE  FUNCTION                             *
 ****************************************************************************/

/*
 * Calculate the average time waiting for the process to finish
 */
float avgTimeWait(vector<process> result, unsigned int  &max_wait_time) {
    unsigned long int total_time = 0;
    unsigned int      total_ps   = 0;
    float             avg_time   = 0;
    int               last_pid   = 0;

    for (int i = 1; i < result.size(); i++) {
        // if the process is terminated 
        if (result[i].exe_time == 0) {
			unsigned int p_wait = result[i].trm_time - result[i].arv_time - result[i].bst_time;
            total_time += p_wait;
			total_ps += 1;
			if(max_wait_time < p_wait) {
				max_wait_time = p_wait;
			}
        }
    }

    avg_time = (float) total_time/total_ps;

    return avg_time;
}

/*
 * Calculate context switch time 
 */
unsigned int contextSwitch(vector<process> result) {
    unsigned long int total_time = 0;
    unsigned int      total_ps   = 0;
    float             avg_time   = 0;
    int               last_pid   = 0;

    for (int i = 1; i < result.size(); i++) {
        // if the process has been changed
        if (last_pid != result[i].pid) {
            total_time += 1;
            last_pid = result[i].pid;
        }
    }

    avg_time = (float) total_time/total_ps;

    return avg_time;
}

/*
 * Calculate average reacting time 
 */
float reactTime(vector<process> result, const unsigned int data_size) {
	vector< int > ans(data_size+1, -1);
    unsigned long int total = 0;
    float             avg_rep   = 0;

    for (int i = 0; i < result.size(); i++) {
        
		unsigned long long int res_time = result[i].trm_time - result[i].arv_time - result[i].bst_time + result[i].exe_time;
		if (ans[result[i].pid] > res_time) {
			ans[result[i].pid] = res_time;
		}
		if(result[i].pid == data_size+1) {break;}
    }
	for (int i = 0; i < ans.size(); i++) {
		total += ans[i];
	}
	
	avg_rep = (float) total/ans.size();

    return avg_rep;
}

/*
 * Calculate the turn around time for the process to finish
 */
float turnAround(vector<process> result) {
    unsigned long int total_time = 0;
    unsigned int      total_ps   = 0;
    float             avg_time   = 0;
    int               last_pid   = 0;

    for (int i = 1; i < result.size(); i++) {
        // if the process has been changed
        // if (last_pid != result[i].pid) {
        //     total_time += 1;
        //     last_pid = result[i].pid;
        // }

        // if the process is terminated 
        if (result[i].exe_time == 0) {
            total_time += (result[i].trm_time - result[i].arv_time);
			total_ps += 1;
        }

		// record the maximum pid, as which the largest pid is the total process
        // if (total_ps < result[i].pid && result[i].pid > 0)
        //     total_p = result[i].pid;
    }

    avg_time = (float) total_time/total_ps;

    return avg_time;
}

/*
 * Calculate the turn around time for the process to finish
 */
float totalThroughPut(vector<process> result) {
    unsigned long int total_time = 0;
    unsigned int      total_ps  = 0;
    float             avg_time  = 0;
    int               last_pid  = 0;

    for (int i = 0; i < result.size(); i++) {
        // if the process is completed, counts
        if (result[i].exe_time == 0) {
			total_ps += 1;
        }
    }

    total_time = result[result.size() - 1].trm_time;
    avg_time = (float) total_ps/total_time;

    return avg_time;
}

/*
 * Calculate the turn around time for the process to finish
 */
vector<double> throughPut(vector<process> result) {
	vector<double> ans;
    unsigned int      total_ps  = 0;
    double            tmpTP  = 0;
	long long int 	  time = -1;

    for (int i = 0; i < result.size(); i++) {
		// keep calculating the throughput as the time increase
		while(ans.size() < result[i].trm_time) {
			time++;
			tmpTP = (float)total_ps/time;
			ans.push_back(tmpTP);
		}
        // if the process is completed, counts
        if (result[i].exe_time == 0) {
			total_ps += 1;
        }
		// push back the new through put
		time++;
		tmpTP = (float)total_ps/time;
		ans.push_back(tmpTP);
    }

    return ans;
}

/*
 * Calculate the waiting time as time increase
 */
void waitTime(vector<process> result, vector<vector<unsigned int> > &ans) {
	vector<unsigned int> tmp(2);
    unsigned long int      wait_time  = 0;
    unsigned int      total_ps   = 0;
    unsigned int      tmpWT  = 0;
	long long int 	  time = -1;

    for (int i = 0; i < result.size(); i++) {
		// keep calculating the throughput as the time increase
		
        // if the process is completed, counts
        if (result[i].exe_time == 0) {
			wait_time = result[i].trm_time - result[i].arv_time - result[i].bst_time;
			tmp[0] = result[i].pid;
			tmp[1] = wait_time;
			ans.push_back(tmp);
        }
		// push back the new through put
    }

    // return ans;
}

/****************************************************************************
 *                          OPERATION  FUNCTION                             *
 ****************************************************************************/

/*
 * Sort from large to small number
 */
bool compare_priority(process p1, process p2) {
	if (p1.priority > p2.priority) {
		return true;
	}
	else if (p1.priority == p2.priority) {
		// Need to turn id into integer before comparation
        return p1.pid > p2.pid;
	}
	else {
		return false;
	}
}

/* 
 * Insertion sort for ready queue, for the number are nearly sorted
 */
void insertAndSort(vector<process> &vec, process new_member) {
    vec.push_back(new_member);
    for(int choose = vec.size()-2; choose >= 0; choose--) {
		if(vec[choose].exe_time == new_member.exe_time && vec[choose].arv_time > new_member.arv_time) {
			giveVal(vec[choose+1], new_member);
			break;
		}
        else if (vec[choose].exe_time > new_member.exe_time) {
            giveVal(vec[choose+1], new_member);
            break;
        }
        else{
            giveVal(vec[choose+1], vec[choose]);
            if(choose == 0) {
                giveVal(vec[choose], new_member);
                break;
            }
        }
    }
}

/*
 * Insertion sort for ready queue, for the number are nearly sorted
 * Sort from large -> small
 */
void InsertionSort(vector<process> &ready) {
	//start from the second element
	for (int i = 1; i < ready.size(); i++) {
		process key = ready[i];

		//compare with 1~i-1 elements
		int j = i - 1;
		//if the element before is smaller, then move backward.
		while (j >= 0 && ready[j].priority <= key.priority) {
			if (ready[j].priority == key.priority) {
				if (ready[j].pid > key.pid) {
					break;
				}
			}
			giveVal(ready[j + 1], ready[j]);
			j = j - 1;
		}
		//until the element before is smaller, then new element sits after it.
		giveVal(ready[j + 1], key);
	}
}

/*
 * Let p1 = p2
 */
void giveVal(process &p1, process &p2) {
    p1.pid      = p2.pid;
    p1.arv_time = p2.arv_time;
    p1.exe_time = p2.exe_time;
    p1.priority = p2.priority;
    p1.trm_time = p2.trm_time;
	p1.bst_time = p2.bst_time;
}

/*
 * Used to convert string pid to interger
 */
int turnToNum(string s) {
	int ans = 0;
	string id = s.substr(1, s.size() - 1);
	ans = atoi(id.c_str());
	// cout<<ans<<endl;
	return ans;
}

/*
 * Read data from file, and return the data queue
 */
void readFile(string in_file_name, queue<process> &input_data) {
    fstream file_in;
    process ps;
	string tmp;
    //cout<<"readfile, before open\n";
    file_in.open(in_file_name, ios::in);
    if (!file_in) {
        cerr << "Error: File open failed.\n";
        exit(1);
    }
	
    //cout<<"readfile, after open\n";
    while (file_in >> tmp >> ps.priority >> ps.exe_time >> ps.arv_time) {
		//cout<<tmp<<tmp.size()<<endl;
		ps.pid = turnToNum(tmp);
        ps.trm_time = -1;
		ps.bst_time = ps.exe_time;
        input_data.push(ps);
    }
    file_in.close();
}

/*
 * Save information to output file
 */
void saveToFile(string out_file_name, vector<process> &sched) {

    fstream file_out;

	// read the file and write begin at its end
    // file_out.open(out_file_name, ios::out | ios::app);
    file_out.open(out_file_name, ios::out);

    if (!file_out) {
        cerr << "Error: File open failed.\n";
        exit(1);
    }

	file_out << "SEQ,PID,PRY,EXE,ARV,BST,TRM\n";

    for (int i = 0; i < sched.size(); i++) {
        file_out << i << "," << sched[i].pid << "," << sched[i].priority << "," ;
        file_out << sched[i].exe_time << "," << sched[i].arv_time << "," << sched[i].bst_time << "," << sched[i].trm_time << endl;
    }
        
    file_out.close();
}

/*
 * Save throughput to output file
 */
void saveTP(string out_file_name, vector<double> &sched) {

    fstream file_out;

	// read the file and write begin at its end
    // file_out.open(out_file_name, ios::out | ios::app);
    file_out.open(out_file_name, ios::out);

    if (!file_out) {
        cerr << "Error: File open failed.\n";
        exit(1);
    }

	file_out << "T,TP\n";

    for (int i = 0; i < sched.size(); i++)
    {	
		double tmp = (float) sched[i];
		file_out << i << "," << fixed << std::setprecision(8) << tmp << "\n";
    }
        
    file_out.close();
}

/*
 * Save waiting time to output file
 */
void saveWT(string out_file_name, vector<vector<unsigned int> > &sched) {

    fstream file_out;

	// read the file and write begin at its end
    // file_out.open(out_file_name, ios::out | ios::app);
    file_out.open(out_file_name, ios::out);

    if (!file_out) {
        cerr << "Error: File open failed.\n";
        exit(1);
    }

	file_out << "TP,WT\n";

    for (int i = 0; i < sched.size(); i++)
    {	
		file_out << sched[i][0] << "," << sched[i][1] << "\n";
    }
        
    file_out.close();
}
