/*
 * Copy Right Reserved.
 * 
 * NCKU EE Department, OS
 * 
 * Date: 2019/05/03
 */

#include <iostream>
#include <fstream>
#include <queue>
#include <vector>
#include <algorithm>
#include <math.h>
#include <string>
// #include <bits/stdc++.h> // for vector sorting

#define INPUTDATA  "data_3.txt"
#define OUTPUTDATA "output.txt"

using namespace std;

typedef struct process {
    int pid;   // process id
    int priority;
    int exe_time; // execution time
    int arv_time; // arriving time
    int trm_time; // terminated time
	int bst_time; // CPU burst time
} process;

/*
 * Used to convert string pid to interger
 */
int turnToNum(string s) {
	int length  = s.size() - 1;
	//cout<<s<<s.size()<<length<<endl;
	int ans = 0;
	for(int i = 1; i<s.size(); i++ ) {
		int tmp = s[i];
		ans = ans + ((tmp-48)*pow(10, length-i));
		//cout<<i<<endl;
	}
	//cout<<ans<<endl;
	return ans;
}

/*
 * Read data from file, and return the data queue
 */
void readFile(string in_file_name, queue<process> &input_data) {
    fstream file_in;
    process ps;
	string tmp;
    //cout<<"readfile, before open\n";
    file_in.open(in_file_name, ios::in);
    if (!file_in) {
        cerr << "Error: File open failed.\n";
        exit(1);
    }
	
    //cout<<"readfile, after open\n";
    while (file_in >> tmp >> ps.priority >> ps.exe_time >> ps.arv_time) {
		//cout<<tmp<<tmp.size()<<endl;
		ps.pid = turnToNum(tmp);
        ps.trm_time = 0;
		ps.bst_time = ps.exe_time;
        input_data.push(ps);
    }
    file_in.close();
}

/*
 * Save information to output file
 */
void saveToFile(string out_file_name, vector<process> &sched) {
    fstream file_out;
    file_out.open(out_file_name, ios::out);
    if (!file_out) {
        cerr << "Error: File open failed.\n";
        exit(1);
    }
	file_out << "T\tPID\tPRY\tEXE\tARV\tBST\tTRM\n";
    for (int i = 0; i < sched.size(); i++)
    {
        file_out << i+1 << "\tP" << sched[i].pid << "\t" << sched[i].priority << "\t" ;
        file_out << sched[i].exe_time << "\t" << sched[i].arv_time << "\t" << sched[i].bst_time << "\t" << sched[i].trm_time << endl;
    }
    
    file_out.close();
}

/*
 * Let p1 = p2
 */
void giveVal(process &p1, process &p2) {
    p1.pid      = p2.pid;
    p1.arv_time = p2.arv_time;
    p1.exe_time = p2.exe_time;
    p1.priority = p2.priority;
    p1.trm_time = p2.trm_time;
	p1.bst_time = p2.bst_time;
}

/*
 * Insertion sort for ready queue, for the number are nearly sorted
 */
void insertAndSort(vector<process> &vec, process new_member) {
    vec.push_back(new_member);
    for(int choose = vec.size()-2; choose >= 0; choose--) {
        if (vec[choose].exe_time > new_member.exe_time) {
            // giveVal(vec[choose], vec[choose+1]);
            // giveVal(new_member, vec[choose+1]);
            giveVal(vec[choose+1], new_member);
            break;
        }
        else{
            giveVal(vec[choose+1], vec[choose]);
            if(choose == 0) {
                giveVal(vec[choose], new_member);
                break;
            }
        }
    }
}


/*
 * Shortest-Job-Next Scheduling Algorithm
 * The job which has the shortest execute time run first
 */
void SJN(queue<process> data, vector<process> &result) {
    vector<process> ready;        //Simulate ready queue
    process exec = {-1, 0, 0, 0, 0, -1};
	process emt = {-1, 0, 0, 0, 0, -1}; // empty execution process
	
	//cout<<"before process\n";
    int NOW = 0;
    while(!data.empty() || !ready.empty() || exec.exe_time != 0) {
        NOW++;
        exec.trm_time = NOW;
		//cout<<NOW<<endl;
		if (exec.exe_time > 0) 
			exec.exe_time--;
		
		// if (exec.exe_time == 0 && exec.pid > 0) {
		// 	exec.trm_time = NOW;
		//     result.push_back(exec); 
		// }
		
        while(!data.empty() && data.front().arv_time == NOW) {
			
			//cout<<"in popping process\n";
            insertAndSort(ready, data.front());
            data.pop();
        }

        if (exec.exe_time == 0) {
            if (exec.pid > 0) {
                result.push_back(exec); 
            }
			giveVal(exec, emt);
			if (ready.size() > 0) {
				// move process from ready queue to execute
				giveVal(exec, ready[ready.size() - 1]);
				// exec.bst_time = NOW;
				ready.pop_back();
			}
        }
    }
}

int main() {

    queue<process> data;
    vector<process> sched;   // record the scheduled result

    /********************** Read Porcesses Information ************************/
    
    readFile(INPUTDATA, data);
        
    /****************************** Scheduling ********************************/

    SJN(data, sched);
    // output the scheduled result
    saveToFile(OUTPUTDATA, sched);

    return 0;
}
