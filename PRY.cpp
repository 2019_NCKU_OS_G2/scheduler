/*
 * Copy Right Reserved.
 *
 * NCKU EE Department, OS
 *
 * Date: 2019/05/08
 */

#include <iostream>
#include <fstream>
#include <queue>
#include <vector>
#include <algorithm>
#include <string>
 // #include <bits/stdc++.h> // for vector sorting

#define INPUTDATA  "data_2.txt"
#define OUTPUTDATA "output.txt"

using namespace std;

typedef struct process {
	int pid;   // process id
	int priority;
	int exe_time; // rest execution time
	int arv_time; // arriving time
	int trm_time; // terminated time
	int bst_time; // total execution time
} process;

/*
 * Used to convert string pid to interger
 */
int turnToNum(string s) {
	string id = s.substr(1, s.size() - 1);
	return atoi(id.c_str());
}

/*
 * Let p1 = p2
 */
void giveVal(process &p1, process &p2) {
	p1.pid = p2.pid;
	p1.arv_time = p2.arv_time;
	p1.bst_time = p2.bst_time;
	p1.priority = p2.priority;
	p1.trm_time = p2.trm_time;
	p1.exe_time = p2.exe_time;
}

/*
 * Insertion sort for ready queue, for the number are nearly sorted
 * Sort from large -> small
 */
void InsertionSort(vector<process> &ready) {
	//start from the second element
	for (int i = 1; i < ready.size(); i++) {
		process key = ready[i];

		//compare with 1~i-1 elements
		int j = i - 1;
		//if the element before is smaller, then move backward.
		while (j >= 0 && ready[j].priority <= key.priority) {
			if (ready[j].priority == key.priority) {
				if (ready[j].pid > key.pid) {
					break;
				}
			}
			giveVal(ready[j + 1], ready[j]);
			j = j - 1;
		}
		//until the element before is smaller, then new element sits after it.
		giveVal(ready[j + 1], key);
	}
}

/*
 * Sort from large to small number
 */
bool compare_priority(process p1, process p2) {
	if (p1.priority > p2.priority) {
		return true;
	}
	else if (p1.priority == p2.priority) {
		/*// Need to turn id into integer before comparation
		string id1 = p1.pid.substr(1, p1.pid.size()-1);
		string id2 = p2.pid.substr(1, p2.pid.size() - 1);*/
		return (p1.pid > p2.pid);
	}
	else {
		return false;
	}
}

/*
 * Read data from file, and return the data queue
 */
void readFile(string in_file_name, queue<process> &input_data) {
	fstream file_in;
	process ps;
	string tmp;	// use for "P" of Pid

	file_in.open(in_file_name, ios::in);
	if (!file_in) {
		cerr << "Error: File open failed.\n";
		exit(1);
	}

	//cout<<"readfile, after open\n";
	while (file_in >> tmp >> ps.priority >> ps.exe_time >> ps.arv_time) {
		ps.pid = turnToNum(tmp);
		ps.trm_time = -1;
		ps.bst_time = ps.exe_time;
		input_data.push(ps);
	}
	file_in.close();
}

/*
 * Save information to output file
 */
void saveToFile(string out_file_name, vector<process> &sched) {
	fstream file_out;
	file_out.open(out_file_name, ios::out);
	if (!file_out) {
		cerr << "Error: File open failed.\n";
		exit(1);
	}
	file_out << "t\tpid\tpry\texe\tarv\tbst\ttrm\n";
	for (int i = 0; i < sched.size(); i++)
	{
		file_out << i << "\tP" << sched[i].pid << "\t" << sched[i].priority << "\t";
		file_out << sched[i].exe_time << "\t" << sched[i].arv_time << "\t" << sched[i].bst_time << "\t" << sched[i].trm_time << endl;
	}
	file_out.close();
}


/*
 * Priority Scheduling Algorithm
 * The job which has the smallest priority run first
 */
void PRY(queue<process> data, vector<process> &sched) {
	vector<process> ready;
	process        ps;

	// init terminate time as -1 to avoid pushing an empty process to sched Q
	int time_slot = -1;
	process running_ps = { 0, 0, 0, 0, 0, 0 };

	// execute all the processes & jump out if running process has terminated
	while (!data.empty() || !ready.empty() || running_ps.exe_time != 0) {
		// as time goes
		time_slot++;

		// decrease the execute time of runnung process
		if (running_ps.exe_time > 0) {
			running_ps.exe_time--;
		}
		// if it terminate, pop out and record it
		if (running_ps.exe_time == 0 && running_ps.trm_time == -1) {
			running_ps.trm_time = time_slot;
			sched.push_back(running_ps);
		}

		// if a new process arrives in this time slot, put it in ready queue
		while (!data.empty() && data.front().arv_time == time_slot) {
			ps = data.front();
			ready.push_back(ps);
			data.pop();
		}

		// if the ready queue is not empty, select the process with the smallest priority to execute
		if (!ready.empty() && running_ps.exe_time <= 0) {
			//  Move the smallest priority one to the last one of vector for pop_back
			InsertionSort(ready);
			// sort(ready.begin(), ready.end(), compare_priority);
			if (ready.back().arv_time <= time_slot) {
				running_ps = ready.back();
				ready.pop_back();
			}
		}
	}
}

int main() {

	queue<process> data;
	vector<process> sched;   // record the scheduled result

	/********************** Read Porcesses Information ************************/

	readFile(INPUTDATA, data);

	/****************************** Scheduling ********************************/

	PRY(data, sched);

	// output the scheduled result
	saveToFile(OUTPUTDATA, sched);

	return 0;
}
