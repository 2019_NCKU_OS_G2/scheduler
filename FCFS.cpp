/*
 * Copy Right Reserved.
 *
 * NCKU EE Department, OS
 *
 * Date: 2019/05/03
 */

#include <iostream>
#include <fstream>
#include <queue>
#include <vector>
#include <algorithm>
#include <cmath>
#include <string>

#define INPUTDATA  "data_1.txt"
#define OUTPUTDATA "output.txt"

using namespace std;

typedef struct process {
	int pid;					// process id
	int priority;
	int exe_time;				// remaining execution time
	int arv_time;				// arriving time
	int trm_time;				// terminated time
	int bst_time;				// total execution time
} process;

bool compare_priority(process p1, process p2)
{
	return (p1.priority < p2.priority);
}

/*
 * Used to convert string pid to interger
 */
int turnToNum(string s)
{
	int length = s.size() - 1;
	int ans = 0;
	for (int i = 1; i < s.size(); i++) {
		int tmp = s[i];
		ans = ans + ((tmp - 48) * pow(10, length - i));
	}
	return ans;
}

/*
 * Read data from file, and return the data queue
 */
void readFile(string in_file_name, queue < process > &input_data)
{
	fstream file_in;
	process ps;
	string tmp;

	file_in.open(in_file_name, ios::in);
	if (!file_in) {
		cerr << "Error: File open failed.\n";
		exit(1);
	}

	while (file_in >> tmp >> ps.priority >> ps.exe_time >> ps.arv_time) {
		ps.pid = turnToNum(tmp);
		ps.trm_time = 0;
		ps.bst_time = ps.exe_time;
		input_data.push(ps);
	}
	file_in.close();
}

/*
 * Save information to output file
 */
void saveToFile(string out_file_name, vector < process > &sched)
{
	fstream file_out;
	file_out.open(out_file_name, ios::out);
	if (!file_out) {
		cerr << "Error: File open failed.\n";
		exit(1);
	}
	file_out << "T\tPID\tPRY\tEXE\tARV\tBST\tTRM\n";
	for (int i = 0; i < sched.size(); i++) {
		file_out << i +
			1 << "\tP" << sched[i].pid << "\t" << sched[i].
			priority << "\t";
		file_out << sched[i].exe_time << "\t" << sched[i].
			arv_time << "\t" << sched[i].bst_time << "\t" << sched[i].
			trm_time << endl;
	}

	file_out.close();
}

void FCFS(queue < process > &data, vector < process > &sched)
{
	queue < process > ready;
	vector < process > arrive;	// record the process arrives at the same time
	process ps;					// temp process for priority sort

	int time_slot = -1;
	process running_ps = { 0, 0, 0, 0, -1, 0 };
	// init terminate time as -1 to avoid pushing an empty process to sched Q

	// execute all the processes & jump out if running process has terminated
	while (!data.empty() || !ready.empty()
		   || running_ps.trm_time > time_slot) {

		// as time goes
		time_slot++;
		running_ps.exe_time =
			(running_ps.exe_time > 0) ? (running_ps.exe_time - 1) : 0;

		// if a new process arrives in this time slot, put it in ready queue
		// Note: The same arriving time may occur
		while (!data.empty() && data.front().arv_time == time_slot) {
			ps = data.front();
			arrive.push_back(ps);
			data.pop();
		}

		// if several ps arrive at the same time slot, sort based on priority
		switch (arrive.size()) {
			case 0:
				break;
			case 1:
				ps = arrive.at(0);
				ready.push(ps);
				arrive.clear();
				break;
			default:
				// cerr << "Multi-arriving at " << arrive.at(1).pid << "\n";
				sort(arrive.begin(), arrive.end(), compare_priority);
				for (int i = 0; i < arrive.size(); i++) {
					ps = arrive.at(i);
					ready.push(ps);
				}
				arrive.clear();
		}

		// if a running process terminate, record it
		if (running_ps.trm_time == time_slot) {
			sched.push_back(running_ps);
			// cout << running_ps.pid << " terminated.\n";
		}
		// if the ready queue is not empty, execute the first process in it
		if (!ready.empty() && running_ps.trm_time <= time_slot) {
			if (ready.front().arv_time <= time_slot) {
				running_ps = ready.front();
				running_ps.trm_time = time_slot + running_ps.bst_time;
				ready.pop();
			}
		}
	}
}


int main()
{

	// ifstream		  iFile;
	queue < process > data;
	vector < process > sched;	// record the scheduled result

	readFile(INPUTDATA, data);

	FCFS(data, sched);

	saveToFile(OUTPUTDATA, sched);

	return 0;
}
