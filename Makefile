OS_NAME = $(shell uname -s)

ifeq ($(OS_NAME), Linux)
CC := g++
endif

ifeq ($(OS_NAME), Darwin)
CC := g++-8
endif

exe := FCFS SJN SRTF PRY RR analyze
obj := FCFS.o SJN.o SRTF.o PRY.o RR.o analyze.o


all: $(exe)

$(exe):$(obj)
	$(CC) -o $@.out $@.o
%.o: %.cpp
	$(CC) -c $^ -o $@


exec:
	make
	./$(exe)

.PHONY:clean

clean:
		rm -rf $(obj) $(exe) *.out output.txt *.csv
