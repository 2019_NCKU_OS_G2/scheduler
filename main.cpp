/*
 * Copy Right Reserved.
 * 
 * NCKU EE Department, OS
 * 
 * Date: 2019/05/03
 */

#include <iostream>
#include <fstream>
#include <queue>

#define INPUTDATA "data_1.txt"

using namespace std;

typedef struct process {
    string pid;      // process id
    int priority;
    int exe_time; // execution time
    int arv_time; // arriving time
} process;

int main() {

    ifstream       iFile;
    queue<process> data;
    process        ps;

    /********************** Read Porcesses Information ************************/
    
	iFile.open(INPUTDATA, ios::in);
    if (!iFile) {
        cerr << "Eorror: File open failed.\n";
        exit(1);
    }

	// read the data and construct the data queue
	// Note the syntax prevent the double-reading of the last line in file.
	while (iFile >> ps.pid >> ps.priority >> ps.exe_time >> ps.arv_time) {
        data.push(ps);
    }

    iFile.close();

    while (!data.empty()) {
        cout << data.front().pid << " " << data.front().priority << " "
             << data.front().exe_time << " " << data.front().arv_time << "\n";
        data.pop();
    }

    return 0;
}
